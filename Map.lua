if not Map then Map = {} end
local Addon = Map
Addon.Settings = {}

local LibGUI = LibStub("LibGUI")
local LibColorDialog = LibStub("LibColorDialog")

local boolString = {[true] = "+", [false] = "-", ["+"] = true, ["-"] = false}
local currentZoneId = 0

local YakBorderSets = {["Map_YakBorderForegroundSet1"] = true, ["Map_YakBorderForegroundSet2"] = true, ["Map_YakBorderForegroundSet3"] = true, ["Map_YakBorderForegroundSet4"] = true, ["Map_YakBorderForegroundSet5"] = true}
local Offsets = {	-- disgusting
	["Map_YakBorderForegroundSet1"] = {topleft_x = 10, topleft_y = 10, bottomright_x = -10, bottomright_y = -10},
	["Map_YakBorderForegroundSet2"] = {topleft_x = 10, topleft_y = 10, bottomright_x = -10, bottomright_y = -10},
	["Map_YakBorderForegroundSet3"] = {topleft_x = 10, topleft_y = 10, bottomright_x = -10, bottomright_y = -10}, 
	["Map_YakBorderForegroundSet4"] = {topleft_x = 10, topleft_y = 10, bottomright_x = -10, bottomright_y = -10},
	["Map_YakBorderForegroundSet5"] = {topleft_x = 10, topleft_y = 10, bottomright_x = -10, bottomright_y = -10},
	["EA_FullResizeImage_TanBorder"] = {topleft_x = 4, topleft_y = 4, bottomright_x = -4, bottomright_y = -4},
}

local oldClickMap
function Addon.Initialize()
	if next(Addon.Settings) == nil then
		Addon.Settings.MiniMapHook = true
		Addon.Settings.Map = {}
		Addon.Settings.Map.a = 1
		Addon.Settings.Border = {}
		
		if yakuiVar then
			Addon.Settings.Border.Background ={}
			Addon.Settings.Border.Background.a = yakuiVar.Panel.fxalpha
			Addon.Settings.Border.Background.r = yakuiVar.Colors.colprintr
			Addon.Settings.Border.Background.g = yakuiVar.Colors.colprintg
			Addon.Settings.Border.Background.b = yakuiVar.Colors.colprintb
			Addon.Settings.Border.Foreground ={}
			Addon.Settings.Border.Foreground.r = yakuiVar.Colors.fx.r
			Addon.Settings.Border.Foreground.g = yakuiVar.Colors.fx.g
			Addon.Settings.Border.Foreground.b = yakuiVar.Colors.fx.b
			Addon.Settings.Border.Foreground.a = yakuiVar.Panel.fxalpha
			Addon.Settings.Border.Foreground.fri = "Map_YakBorderForegroundSet5"
			
		else
			Addon.Settings.Border.Background ={}
			Addon.Settings.Border.Background.r = 255
			Addon.Settings.Border.Background.g = 255
			Addon.Settings.Border.Background.b = 255
			Addon.Settings.Border.Background.a = 1
			Addon.Settings.Border.Foreground ={}
			Addon.Settings.Border.Foreground.r = 255
			Addon.Settings.Border.Foreground.g = 255
			Addon.Settings.Border.Foreground.b = 255
			Addon.Settings.Border.Foreground.a = 1
			Addon.Settings.Border.Foreground.fri = "EA_FullResizeImage_TanBorder"
		end
	end
	if not Addon.Settings.Combat then Addon.Settings.Combat = {["+"] = "-",["-"] = "-"} end
	
	if LibSlash then
		if not LibSlash.IsSlashCmdRegistered("zm") then
			LibSlash.RegisterSlashCmd("zm", function(args) Addon.SlashCommand(args) end)
		end
		if not LibSlash.IsSlashCmdRegistered("m") then
			LibSlash.RegisterSlashCmd("m", function(args) Addon.SlashCommand(args) end)
		end
		LibSlash.RegisterSlashCmd("map", function(args) Addon.SlashCommand(args) end)
		LibSlash.RegisterSlashCmd("zonemap", function(args) Addon.SlashCommand(args) end)
	end
	
	CreateWindow("Map",true)
	WindowRegisterEventHandler("Map", SystemData.Events.LOADING_END, "Map.UpdateMap")
	WindowRegisterEventHandler("Map", SystemData.Events.PLAYER_ZONE_CHANGED, "Map.UpdateMap")
	WindowRegisterEventHandler("Map", SystemData.Events.INTERFACE_RELOADED, "Map.InterfaceReloaded")
	
	CreateMapInstance("MapDisplay", SystemData.MapTypes.NORMAL)

	WindowSetLayer("Map", 1)
	Addon.UpdateFilters()
	
	WindowSetAlpha("Map", Addon.Settings.Map.a)
	
	local fgfri = Addon.Settings.Border.Foreground.fri
	
	WindowClearAnchors("MapDisplay")
	local offsets = Offsets[fgfri]
	WindowAddAnchor("MapDisplay", "topleft", "Map", "topleft", offsets.topleft_x , offsets.topleft_y)
	WindowAddAnchor("MapDisplay", "bottomright", "Map", "bottomright", offsets.bottomright_x, offsets.bottomright_y)	
	
	
	if YakBorderSets[fgfri] then
		CreateWindowFromTemplate("MapBorderBackground", "Map_YakBorderBackground", "Map")
		WindowSetAlpha("MapBorderBackground", Addon.Settings.Border.Background.a)
		WindowSetTintColor("MapBorderBackground", Addon.Settings.Border.Background.r, Addon.Settings.Border.Background.g, Addon.Settings.Border.Background.b)
	end
	
	CreateWindowFromTemplate(fgfri, fgfri, "Map")
	if not YakBorderSets[fgfri] then	-- them are already
		WindowClearAnchors(fgfri)
		WindowAddAnchor(fgfri, "topright", "Map", "topright", 0, 0)
		WindowAddAnchor(fgfri, "bottomleft", "Map", "bottomleft", 0, 0)
	end
	WindowSetShowing(fgfri, true)
	WindowSetAlpha(fgfri, Addon.Settings.Border.Foreground.a)
	WindowSetTintColor(fgfri, Addon.Settings.Border.Foreground.r, Addon.Settings.Border.Foreground.g, Addon.Settings.Border.Foreground.b)

	--WindowSetHandleInput("MapBorder",false)
	--WindowSetLayer("MapBorder",0)	
	--WindowSetShowing("MapBorder", true)
	if MapMonsterAPI then MapMonsterAPI.RegisterMainMapDisplay("MapDisplay") end
	LayoutEditor.RegisterWindow("Map", L"Map" , L"A simple map.", false, false, true)
	
	RegisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "Map.OnCombatFlagUpdated")
	
	if MapUtils then
		oldClickMap = MapUtils.ClickMap
		MapUtils.ClickMap =
			function(mapDisplay, points)
				oldClickMap(mapDisplay, points)
				if Addon.Settings.MiniMapHook and mapDisplay ~= "EA_Window_WorldMapZoneViewMapDisplay" then Addon.ToggleMap() end
			end
	end
end

function Addon.Shutdown()
	LayoutEditor.UnregisterWindow("Map")
	WindowUnregisterEventHandler("Map", SystemData.Events.LOADING_END)
	WindowUnregisterEventHandler("Map", SystemData.Events.PLAYER_ZONE_CHANGED)
	RemoveMapInstance("MapDisplay")
end


--[[local ignoreMapDisplay = {
	["MapDisplay"] = false,
	["MinmapMapDisplay"] = false,
	["EA_Window_OverheadMapMapDisplay"] = false,
}]]--

--
--
--

function Addon.SetMapZone(zoneId)
	currentZoneId = zoneId
	MapSetMapView( "MapDisplay", GameDefs.MapLevel.ZONE_MAP, zoneId)
	if MapMonsterAPI then MapMonsterAPI.RequestDisplayPinsForZone("MapDisplay", zoneId) end
end

function Addon.UpdateMap()
	Addon.SetMapZone(GameData.Player.zone)
end

function Addon.InterfaceReloaded()
	Addon.UpdateMap()
	WindowSetShowing("Map", false)
end

function Addon.OnClickMap(flags, mapX, mapY)
	if not (MapMonster and MapMonster.DoClickMap("MapDisplay", currentZoneId, flags, mapX, mapY)) then
		if not (MMNavigator and MMNavigator.DoClickMap("MapDisplay", currentZoneId, flags, mapX, mapY)) then
			MapUtils.ClickMap( "MapDisplay", MapDisplay.MouseoverPoints )
		end
	end
end

function Addon.OnRClickMap(flags, mapX, mapY)
	if WARCommander then WARCommander.DoClickMap("MapDisplay",currentZoneId, flags, mapX, mapY) end
end

function Addon.OnMouseOverPoint()
    Tooltips.CreateMapPointTooltip( "MapDisplay", MapDisplay.MouseoverPoints, Tooltips.ANCHOR_CURSOR, Tooltips.MAP_TYPE_MAINMAP )    
end

function Addon.OnMButtonUp()
	EA_Window_ContextMenu.CreateDefaultContextMenu("Map")
end

function Addon.ToggleMap(show)
	if show ~= true and show ~= false then
		show = not WindowGetShowing("Map")
	end
	WindowSetShowing("Map", show)
end

function Addon.UpdateFilters()
	for _, filterType in pairs( SystemData.MapPips ) do
		MapSetPinFilter("MapDisplay", filterType, EA_Window_WorldMap.Settings.mapPinFilters[filterType] )
	end
end

function Addon.OnCombatFlagUpdated()
	if Addon.Settings.Combat[boolString[GameData.Player.inCombat]] ~= "-" then
		Addon.ToggleMap(not GameData.Player.inCombat)
	end
end

--
--	Setters
--

function Addon.SetMapAlpha(value)
	Addon.Settings.Map.a = value
	WindowSetAlpha("Map", value)
end

function Addon.SetBorderBackgroundAlpha(value)
	Addon.Settings.Border.Background.a = value
	if DoesWindowExist("MapBorderBackground") then
		WindowSetAlpha("MapBorderBackground", value)
	end
end

function Addon.SetBorderForegroundAlpha(value)
	Addon.Settings.Border.Foreground.a = value
	WindowSetAlpha(Addon.Settings.Border.Foreground.fri, Addon.Settings.Border.Foreground.a)
end

function Addon.SetBorderBackgroundColor(r,g,b)
	Addon.Settings.Border.Background.r = r
	Addon.Settings.Border.Background.g = g
	Addon.Settings.Border.Background.b = b
	if DoesWindowExist("MapBorderBackground") then
		WindowSetTintColor("MapBorderBackground", r,g,b)
	end
end

function Addon.SetBorderForegroundColor(r,g,b)
	Addon.Settings.Border.Foreground.r = r
	Addon.Settings.Border.Foreground.g = g
	Addon.Settings.Border.Foreground.b = b
	WindowSetTintColor(Addon.Settings.Border.Foreground.fri, r,g,b)
end

function Addon.SetBorderForegroundFRI(newfri)
	if Addon.Settings.Border.Foreground.fri == newfri then return end

	if (YakBorderSets[newfri]) and (not DoesWindowExist("MapBorderBackground")) then
		CreateWindowFromTemplate("MapBorderBackground", "Map_YakBorderBackground", "Map")
		WindowSetAlpha("MapBorderBackground", Addon.Settings.Border.Background.a)
		WindowSetTintColor("MapBorderBackground", Addon.Settings.Border.Background.r, Addon.Settings.Border.Background.g, Addon.Settings.Border.Background.b)
	elseif (not YakBorderSets[newfri]) and (DoesWindowExist("MapBorderBackground")) then
		DestroyWindow("MapBorderBackground")
	end
	
	local oldfri = Addon.Settings.Border.Foreground.fri or ""
	if DoesWindowExist(oldfri) then DestroyWindow(oldfri) end	
	CreateWindowFromTemplate(newfri, newfri, "Map")
	WindowSetShowing(newfri, true)
	WindowSetAlpha(newfri, Addon.Settings.Border.Foreground.a)
	WindowSetTintColor(newfri, Addon.Settings.Border.Foreground.r, Addon.Settings.Border.Foreground.g, Addon.Settings.Border.Foreground.b)
	
	WindowClearAnchors("MapDisplay")
	local offsets = Offsets[newfri]
	WindowAddAnchor("MapDisplay", "topleft", "Map", "topleft", offsets.topleft_x , offsets.topleft_y)
	WindowAddAnchor("MapDisplay", "bottomright", "Map", "bottomright", offsets.bottomright_x, offsets.bottomright_y)
	
	Addon.Settings.Border.Foreground.fri = newfri
end

--
-- Config
--
Addon.FontHeadline = "font_default_war_heading" 
Addon.FontBold = "font_default_medium_heading" --"font_clear_medium_bold"
Addon.FontText = "font_default_text" -- "font_clear_medium"

function Addon.SlashCommand(cmd)
	local opt, value = cmd:match("([a-zA-Z0-9]+)[ ]?(.*)")
	if (nil == opt) then
		EA_ChatWindow.Print(L"Available commands:<br>toggle: Shows and Hides the Map<br>config: Shows the option GUI")
		return
	end
	opt = string.lower(opt)
	if (opt == "toggle") then
		Addon.ToggleMap()
	elseif opt == "config" then
		Addon.ShowSettings()
	else
		EA_ChatWindow.Print(L"Available commands:<br>toggle: Shows and Hides the Map<br>config: Shows the option GUI")
	end
end

local comboFRI = {
	["TanBorder"] = "EA_FullResizeImage_TanBorder",
	["YakSet1"] = "Map_YakBorderForegroundSet1",
	["YakSet2"] = "Map_YakBorderForegroundSet2",
	["YakSet3"] = "Map_YakBorderForegroundSet3",
	["YakSet4"] = "Map_YakBorderForegroundSet4",
	["YakSet5"] = "Map_YakBorderForegroundSet5",
	
	["EA_FullResizeImage_TanBorder"] = "TanBorder",
	["Map_YakBorderForegroundSet1"] = "YakSet1",
	["Map_YakBorderForegroundSet2"] = "YakSet2",
	["Map_YakBorderForegroundSet3"] = "YakSet3",
	["Map_YakBorderForegroundSet4"] = "YakSet4",
	["Map_YakBorderForegroundSet5"] = "YakSet5",
}
function Addon.ShowSettings()
	if not Addon.W then
		local w = LibGUI("Blackframe", "MapConfig")
		w:Resize(480, 520)
		w:MakeMovable()
		w:AnchorTo("Root", "center", "center", 0, 0)
		--w.Background = LibGUI("Window", "Standalone"..WindowSettings.."Background", "EA_Window_DefaultBackgroundFrame")
		w.TitleBar = w:Add("Window", "MapConfigTitleBar", "EA_TitleBar_Default")
		LabelSetText("MapConfigTitleBarText", L"Map Config")
		w.TitleBar:IgnoreInput()
		w.TitleBar:ProcessAnchors()
		w.ButtonClose = w:Add(LibGUI("closebutton"))
		w.ButtonClose.OnLButtonUp = 
			function()
				w:Destroy()
				Addon.W = nil
			end
		
		w.lMiniMapHook = w:Add("Label", w.name.."lMiniMapHook", "MapSettingsAutoresizeLabelTemplate")
		w.lMiniMapHook:Position(20,45)
		w.lMiniMapHook:Font(Addon.FontHeadline)
		w.lMiniMapHook:SetText(L"MiniMap Toggle:")
		w.lMiniMapHook:Align("left")
		
		w.ckMiniMapHook = w:Add("checkbox", w.name.."ckMiniMapHook")
		w.ckMiniMapHook:AnchorTo(w.lMiniMapHook, "right", "left", 5, 0)
		w.ckMiniMapHook.OnLButtonUp =
			function()
				Addon.Settings.MiniMapHook = w.ckMiniMapHook:GetValue()
			end

		w.lHideInCombat = w:Add("Label", w.name.."lHideInCombat", "MapSettingsAutoresizeLabelTemplate")
		w.lHideInCombat:AnchorTo(w.lMiniMapHook, "bottomleft", "topleft", 0, 15)
		w.lHideInCombat:Font(Addon.FontBold)
		w.lHideInCombat:SetText(L"Hide in Combat:")
		w.lHideInCombat:Align("left")
		
		w.cHideInCombat = w("combobox", w.name.."cHideInCombat", "MapSettings_ComboBox_DefaultResizableTiny_Template")
		w.cHideInCombat:AnchorTo(w.lHideInCombat, "right", "left", 5, 0)
		w.cHideInCombat:Add("-")
		w.cHideInCombat:Add("+")
		w.cHideInCombat.OnSelChanged =
			function()
				Addon.Settings.Combat[boolString[true] ] = WStringToString(w.cHideInCombat:Selected())
			end

		w.lShowOutOfCombat = w:Add("Label", w.name.."lShowOutOfCombat", "MapSettingsAutoresizeLabelTemplate")
		w.lShowOutOfCombat:AnchorTo(w.lHideInCombat, "bottomleft", "topleft", 0, 15)
		w.lShowOutOfCombat:Font(Addon.FontBold)
		w.lShowOutOfCombat:SetText(L"Show out of Combat:")
		w.lShowOutOfCombat:Align("left")
		
		w.cShowOutOfCombat = w("combobox", w.name.."cShowOutOfCombat", "MapSettings_ComboBox_DefaultResizableTiny_Template")
		w.cShowOutOfCombat:AnchorTo(w.lShowOutOfCombat, "right", "left", 5, 0)
		w.cShowOutOfCombat:Add("-")
		w.cShowOutOfCombat:Add("+")
		w.cShowOutOfCombat.OnSelChanged =
			function()
				Addon.Settings.Combat[boolString[false] ] = WStringToString(w.cShowOutOfCombat:Selected())
			end
			
		w.lMap = w:Add("Label", w.name.."lMap", "MapSettingsAutoresizeLabelTemplate")
		w.lMap:AnchorTo(w.lShowOutOfCombat, "bottomleft", "topleft", 0, 15)
		w.lMap:Font(Addon.FontHeadline)
		w.lMap:SetText(L"Map:")
		w.lMap:Align("left")
		
		w.lMapAlpha = w:Add("Label", w.name.."lMapAlpha", "MapSettingsAutoresizeLabelTemplate")
		w.lMapAlpha:AnchorTo(w.lMap, "bottomleft", "topleft", 5, 15)
		w.lMapAlpha:Font(Addon.FontBold)
		w.lMapAlpha:SetText(L"Alpha:")
		w.lMapAlpha:Align("left")

		w.tMapAlpha = w("Textbox", w.name.."tMapAlpha")
		w.tMapAlpha:Resize(60)
		w.tMapAlpha:AnchorTo(w.lMapAlpha, "right", "left", 5, 0)
		w.tMapAlpha.OnKeyEnter = function() Addon.SetMapAlpha(tonumber(w.tMapAlpha:GetText())) end
		
		w.lBorder = w:Add("Label", w.name.."lB", "MapSettingsAutoresizeLabelTemplate")
		w.lBorder:AnchorTo(w.lMapAlpha, "bottomleft", "topleft", 0, 15)
		w.lBorder:Font(Addon.FontBold)
		w.lBorder:SetText(L"Border:")
		w.lBorder:Align("left")
		
		w.cBorderFRI = w("combobox", w.name.."cBorderFRI", "MapSettings_ComboBox_DefaultResizable_Template")
		w.cBorderFRI:AnchorTo(w.lBorder, "right", "left", 5, 0)
		w.cBorderFRI:Add("TanBorder")
		w.cBorderFRI:Add("YakSet1")
		w.cBorderFRI:Add("YakSet2")
		w.cBorderFRI:Add("YakSet3")
		w.cBorderFRI:Add("YakSet4")
		w.cBorderFRI:Add("YakSet5")
		--w.cBorderFRI:SetId(comboboxIds[w.cBorderFRI.name])
		w.cBorderFRI.OnSelChanged =
			function()
				Addon.SetBorderForegroundFRI(comboFRI[WStringToString(w.cBorderFRI:Selected())])
			end
			
		w.lBorderFG = w:Add("Label", w.name.."lBFG", "MapSettingsAutoresizeLabelTemplate")
		w.lBorderFG:AnchorTo(w.lBorder, "bottomleft", "topleft", -5, 15)
		w.lBorderFG:Font(Addon.FontHeadline)
		w.lBorderFG:SetText(L"Border Foreground:")
		w.lBorderFG:Align("left")

		w.lBorderFGAlpha = w:Add("Label", w.name.."lBFGAlpha", "MapSettingsAutoresizeLabelTemplate")
		w.lBorderFGAlpha:AnchorTo(w.lBorderFG, "bottomleft", "topleft", 5, 15)
		w.lBorderFGAlpha:Font(Addon.FontBold)
		w.lBorderFGAlpha:SetText(L"Alpha:")
		w.lBorderFGAlpha:Align("left")

		w.tBorderFGAlpha = w("Textbox", w.name.."tBFGAlpha")
		w.tBorderFGAlpha:Resize(60)
		w.tBorderFGAlpha:AnchorTo(w.lBorderFGAlpha, "right", "left", 5, 0)
		w.tBorderFGAlpha.OnKeyEnter = function() Addon.SetBorderForegroundAlpha(tonumber(w.tBorderFGAlpha:GetText())) end
		
		w.lBorderFGColor = w:Add("Label", w.name.."lBFGColor", "MapSettingsAutoresizeLabelTemplate")
		w.lBorderFGColor:AnchorTo(w.lBorderFGAlpha, "bottomleft", "topleft", 0, 15)
		w.lBorderFGColor:Font(Addon.FontBold)
		w.lBorderFGColor:SetText(L"Color:")
		w.lBorderFGColor:Align("left")

		w.cBorderFGColorPresets = w("combobox", w.name.."cBorderFGColorPresets", "MapSettings_ComboBox_DefaultResizable_Template")
		w.cBorderFGColorPresets:AnchorTo(w.lBorderFGColor, "right", "left", 5, 0)
		w.cBorderFGColorPresets:SetEnabled(LibColors ~= nil)
		--w.cBorderFGColorPresets:SetId(comboboxIds[w.cBorderFGColorPresets.name])
		w.cBorderFGColorPresets.OnSelChanged =
			function()
				Addon.Settings.Border.Foreground.ColorPreset = WStringToString(w.cBorderFGColorPresets:Selected())
				local ColorPreset = LibColors.GetColorPreset(Addon.Settings.Border.Foreground.ColorPreset)
				Addon.SetBorderForegroundColor(ColorPreset.r, ColorPreset.g, ColorPreset.b)
				w.colorBoxBorderFGColor:Tint(ColorPreset.r, ColorPreset.g, ColorPreset.b)
			end
			
		w.colorBoxBorderFGColorBG = w("Image", w.name.."iBFGColorBG")
		w.colorBoxBorderFGColorBG:Resize(30,30)
		w.colorBoxBorderFGColorBG:Tint(64, 64, 64)
		w.colorBoxBorderFGColorBG:AnchorTo(w.cBorderFGColorPresets, "right", "left", 5, 0)
		
		w.colorBoxBorderFGColor = w("Image", w.name.."iBFGColor")
		w.colorBoxBorderFGColor:Resize(26,26)
		w.colorBoxBorderFGColor:AnchorTo(w.colorBoxBorderFGColorBG, "topleft", "topleft", 2, 2)

		-- windows and frames registercoreeventhandler does not work for lua as of 1.3.4
		w.colorBoxBorderFGLabelForEvents = w("Label", w.name.."lClickBFGColor")
		w.colorBoxBorderFGLabelForEvents:AnchorTo(w.colorBoxBorderFGColor, "topleft", "topleft", 0, 0)
		w.colorBoxBorderFGLabelForEvents:AddAnchor(w.colorBoxBorderFGColor, "bottomright", "bottomright", 0, 0)
		w.colorBoxBorderFGLabelForEvents.OnLButtonUp =
			function()
				local selection = WStringToString(w.cBorderFGColorPresets:Selected())
				if LibColors ~= nil and selection ~= nil and selection ~= "" and selection ~= "none" then
					LibColors.EditColorPreset(selection)
				else
					local ColorDialogOwner, ColorDialogFunction = LibColorDialog.API_GetLink()
					if ColorDialogOwner ~= Addon or ColorDialogFunction ~= Addon.EditBarPanel.OnColorDialogCallback then
						LibColorDialog.API_OpenDialog(
							Addon,
							function(_, Event, EventData)
								-- Hint: COLOR_EVENT_UPDATED sends the old value again if cancel ist clicked
								if Addon.W and Event == LibColorDialog.Events.COLOR_EVENT_UPDATED then
									Addon.W.colorBoxBorderFGColor:Tint(math.floor(EventData.Red + 0.5), math.floor(EventData.Green + 0.5), math.floor(EventData.Blue + 0.5))
									Addon.SetBorderForegroundColor(math.floor(EventData.Red + 0.5), math.floor(EventData.Green + 0.5), math.floor(EventData.Blue + 0.5))
								end
							end
							, true, 
							Addon.Settings.Border.Foreground.r,
							Addon.Settings.Border.Foreground.g,
							Addon.Settings.Border.Foreground.b,
							1, Window.Layers.SECONDARY, LibColorDialog.ColorTypes.COLOR_TYPE_RGB
						)
					else
						LibColorDialog.API_CloseDialog(true)
					end
				end
			end
	
		w.lBorderBG = w:Add("Label", w.name.."lBBG", "MapSettingsAutoresizeLabelTemplate")
		w.lBorderBG:AnchorTo(w.lBorderFGColor, "bottomleft", "topleft", -5, 15)
		w.lBorderBG:Font(Addon.FontHeadline)
		w.lBorderBG:SetText(L"Border Background:")
		w.lBorderBG:Align("left")
		
		w.lBorderBGAlpha = w:Add("Label", w.name.."lBBGAlpha", "MapSettingsAutoresizeLabelTemplate")
		w.lBorderBGAlpha:AnchorTo(w.lBorderBG, "bottomleft", "topleft", 5, 15)
		w.lBorderBGAlpha:Font(Addon.FontBold)
		w.lBorderBGAlpha:SetText(L"Alpha:")
		w.lBorderBGAlpha:Align("left")

		w.tBorderBGAlpha = w("Textbox", w.name.."tBBGAlpha")
		w.tBorderBGAlpha:Resize(60)
		w.tBorderBGAlpha:AnchorTo(w.lBorderBGAlpha, "right", "left", 5, 0)
		w.tBorderBGAlpha.OnKeyEnter = function() Addon.SetBorderBackgroundAlpha(tonumber(w.tBorderBGAlpha:GetText())) end
		
		w.lBorderBGColor = w:Add("Label", w.name.."lBBGColor", "MapSettingsAutoresizeLabelTemplate")
		w.lBorderBGColor:AnchorTo(w.lBorderBGAlpha, "bottomleft", "topleft", 0, 15)
		w.lBorderBGColor:Font(Addon.FontBold)
		w.lBorderBGColor:SetText(L"Color:")
		w.lBorderBGColor:Align("left")

		w.cBorderBGColorPresets = w("combobox", w.name.."cBorderBGColorPresets", "MapSettings_ComboBox_DefaultResizable_Template")
		w.cBorderBGColorPresets:AnchorTo(w.lBorderBGColor, "right", "left", 5, 0)
		w.cBorderBGColorPresets:SetEnabled(LibColors ~= nil)
		--w.cBorderBGColorPresets:SetId(comboboxIds[w.cBorderBGColorPresets.name])
		w.cBorderBGColorPresets.OnSelChanged =
			function()
				Addon.Settings.Border.Background.ColorPreset = WStringToString(w.cBorderBGColorPresets:Selected())
				local ColorPreset = LibColors.GetColorPreset(Addon.Settings.Border.Background.ColorPreset)
				Addon.SetBorderBackgroundColor(ColorPreset.r, ColorPreset.g, ColorPreset.b)
				w.colorBoxBorderBGColor:Tint(ColorPreset.r, ColorPreset.g, ColorPreset.b)
			end
		
		w.colorBoxBorderBGColorBG = w("Image", w.name.."iBBGColorBG")
		w.colorBoxBorderBGColorBG:Resize(30,30)
		w.colorBoxBorderBGColorBG:Tint(64, 64, 64)
		w.colorBoxBorderBGColorBG:AnchorTo(w.cBorderBGColorPresets, "right", "left", 5, 0)
		
		w.colorBoxBorderBGColor = w("Image", w.name.."iBBGColor")
		w.colorBoxBorderBGColor:Resize(26,26)
		w.colorBoxBorderBGColor:AnchorTo(w.colorBoxBorderBGColorBG, "topleft", "topleft", 2, 2)

		-- windows and frames registercoreeventhandler does not work for lua as of 1.3.4
		w.colorBoxBorderBGLabelForEvents = w("Label", w.name.."lClickBBGColor")
		w.colorBoxBorderBGLabelForEvents:AnchorTo(w.colorBoxBorderBGColor, "topleft", "topleft", 0, 0)
		w.colorBoxBorderBGLabelForEvents:AddAnchor(w.colorBoxBorderBGColor, "bottomright", "bottomright", 0, 0)
		w.colorBoxBorderBGLabelForEvents.OnLButtonUp =
			function()
				local selection = WStringToString(w.cBorderBGColorPresets:Selected())
				if LibColors ~= nil and selection ~= nil and selection ~= "" and selection ~= "none" then
					LibColors.EditColorPreset(selection)
				else
					local ColorDialogOwner, ColorDialogFunction = LibColorDialog.API_GetLink()
					if ColorDialogOwner ~= Addon or ColorDialogFunction ~= Addon.EditBarPanel.OnColorDialogCallback then
						LibColorDialog.API_OpenDialog(
							Addon, 
							function(_, Event, EventData)
								-- Hint: COLOR_EVENT_UPDATED sends the old value again if cancel ist clicked
								if Addon.W and Event == LibColorDialog.Events.COLOR_EVENT_UPDATED then
									Addon.W.colorBoxBorderBGColor:Tint(math.floor(EventData.Red + 0.5), math.floor(EventData.Green + 0.5), math.floor(EventData.Blue + 0.5))
									Addon.SetBorderBackgroundColor(math.floor(EventData.Red + 0.5), math.floor(EventData.Green + 0.5), math.floor(EventData.Blue + 0.5))
								end
							end
							, true,
							Addon.Settings.Border.Background.r,
							Addon.Settings.Border.Background.g,
							Addon.Settings.Border.Background.b,
							1, Window.Layers.SECONDARY, LibColorDialog.ColorTypes.COLOR_TYPE_RGB
						)
					else
						LibColorDialog.API_CloseDialog(true)
					end
				end
			end
			
		Addon.W = w
	end
	
	Addon.W.colorBoxBorderFGColor:Tint(Addon.Settings.Border.Foreground.r, Addon.Settings.Border.Foreground.g, Addon.Settings.Border.Foreground.b)
	Addon.W.cBorderFGColorPresets:Clear()
	Addon.W.cBorderFGColorPresets:Add("none")
	if LibColors ~= nil then
		for _,v in ipairs(LibColors.GetColorPresetList()) do
			Addon.W.cBorderFGColorPresets:Add(v)
		end
		if Addon.Settings.Border.Foreground.ColorPreset ~= nil and Addon.Settings.Border.Foreground.ColorPreset ~= "" and Addon.Settings.Border.Foreground.ColorPreset ~= "none" then
			Addon.W.cBorderFGColorPresets:Select(Addon.Settings.Border.Foreground.ColorPreset)
			local ColorPreset = LibColors.GetColorPreset(Addon.Settings.Border.Foreground.ColorPreset)
			Addon.W.colorBoxBorderFGColor:Tint(ColorPreset.r, ColorPreset.g, ColorPreset.b)
		else
			Addon.W.cBorderFGColorPresets:Select("none")
		end
	end

	Addon.W.colorBoxBorderBGColor:Tint(Addon.Settings.Border.Background.r, Addon.Settings.Border.Background.g, Addon.Settings.Border.Background.b)
	Addon.W.cBorderBGColorPresets:Clear()
	Addon.W.cBorderBGColorPresets:Add("none")
	if LibColors ~= nil then
		for _,v in ipairs(LibColors.GetColorPresetList()) do
			Addon.W.cBorderBGColorPresets:Add(v)
		end
		if Addon.Settings.Border.Background.ColorPreset ~= nil and Addon.Settings.Border.Background.ColorPreset ~= "" and Addon.Settings.Border.Background.ColorPreset ~= "none" then
			Addon.W.cBorderBGColorPresets:Select(Addon.Settings.Border.Background.ColorPreset)
			local ColorPreset = LibColors.GetColorPreset(Addon.Settings.Border.Background.ColorPreset)
			Addon.W.colorBoxBorderBGColor:Tint(ColorPreset.r, ColorPreset.g, ColorPreset.b)
		else
			Addon.W.cBorderBGColorPresets:Select("none")
		end
	end
	
	Addon.W.cBorderFRI:Select(comboFRI[Addon.Settings.Border.Foreground.fri])
	Addon.W.tMapAlpha:SetText(Addon.Settings.Map.a)
	Addon.W.tBorderFGAlpha:SetText(Addon.Settings.Border.Foreground.a)
	Addon.W.tBorderBGAlpha:SetText(Addon.Settings.Border.Background.a)
	if Addon.Settings.MiniMapHook == nil then Addon.Settings.MiniMapHook = false end
	Addon.W.ckMiniMapHook:SetValue(Addon.Settings.MiniMapHook)
	Addon.W.cHideInCombat:Select(Addon.Settings.Combat[boolString[true] ])
	Addon.W.cShowOutOfCombat:Select(Addon.Settings.Combat[boolString[false] ])
	Addon.W:Show()
end

function Addon.OnSelChanged()	-- too lazy to use numbers
	--local id = WindowGetId(SystemData.ActiveWindow.name)
	--Print(string.sub(SystemData.ActiveWindow.name, string.len(WindowGetParent(SystemData.ActiveWindow.name))+ 1))
	Addon.W[string.sub(SystemData.ActiveWindow.name, string.len(WindowGetParent(SystemData.ActiveWindow.name))+ 1)].OnSelChanged()
end
