<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Map" version="1.2.2" date="08/10/2012" >
        
	<Author name="Wothor aka Philosound" email="" />
	<Description text="Map - a less intrusive Worldmap" />
	<VersionSettings gameVersion="1.4.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
	<Dependencies>
		<Dependency name="EA_WorldMapWindow" optional="false" forceEnable="true" />
		<Dependency name="EASystem_Utils" optional="false" forceEnable="true" />
		<Dependency name="LibSlash" optional="true" forceEnable="true" />
		<Dependency name="LibColors" optional="false" forceEnable="true" />
	</Dependencies>
	
	<Files>
		<File name="libs/LibStub.lua" />
		<File name="libs/LibGUI.lua" />
		<File name="Map.xml" />
		<File name="Map.lua" />
	</Files>
        
	<OnInitialize>
		<CallFunction name="Map.Initialize" />
	</OnInitialize> 

	<SavedVariables>
		<SavedVariable name="Map.Settings" />
	</SavedVariables>

	<OnUpdate/>

	<OnShutdown/>   
	<WARInfo>
			<Categories>
				<Category name="MAP" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
	</WARInfo>
	</UiMod>
</ModuleFile>