-- Addon as embedded library. Addon by MrAngel, "librarization" and optimizations by Wothor|Philosound
-- Addon url: http://war.curse.com/downloads/war-addons/details/Addon.aspx original license: MIT License
-- of course, MrAngel is credited via pkgmeta/curseforge settings, to receive the curse points he deserves
-- license: MIT License

-- Actually this causes trouble with versioning iff the .xml needs updating sometime and the lib is used in multiple addons.
-- However, I've really given up explaining to folks that, no, it doesn't make a performance difference if this code is listed as addon or not.
-- Chances are, before the xml would need updating the question is moot with the amount of success of war

-- If you are going to use this, you need to make sure it's in /libs/LibColorDialog (unfortunately, paths are relative to .mod instead of .xml)
-- actually, the easiest and recommended practice would be to just support Global Color Presets

local MAJOR, MINOR = "LibColorDialog", 1
local Addon, oldminor = LibStub:NewLibrary(MAJOR, MINOR)
if not Addon then return end

local WindowColorDialogSettings	= MAJOR.."Settings"
local WindowColorDialog			= MAJOR.."Window"

local CurrentCallbackOwner		= nil
local CurrentCallbackFunction	= nil
local CurrentLayer				= Window.Layers.DEFAULT
local CurrentColorType			= 0
local CurrentColorRed			= 0
local CurrentColorGreen			= 0
local CurrentColorBlue			= 0
local CurrentColorAlpha			= 0
local NewColorRed				= 0
local NewColorGreen				= 0
local NewColorBlue				= 0
local NewColorAlpha				= 0
local NewColorHue				= 0
local NewColorSaturation		= 0
local NewColorBrightness		= 0

local ScreenMouseX				= 0
local ScreenMouseY				= 0

local IsMovingColorPointer		= false
local IsUpdatingEdits			= false

Addon.ColorTypes = {COLOR_TYPE_RGB = 1, COLOR_TYPE_RGBA = 2}
Addon.Events = {COLOR_EVENT_UPDATED	= 1, COLOR_EVENT_CLOSED	= 2}

--------------------------------------------------------------
-- function InitializeWindowColorDialog()
-- Description:
--------------------------------------------------------------
function Addon.InitializeWindowColorDialog()

	-- First step: create main window
	CreateWindow(WindowColorDialog, false)

	-- Second step: set additional controls
	ButtonSetText(WindowColorDialog.."ButtonOK", L"OK")
	ButtonSetText(WindowColorDialog.."ButtonCancel", L"Cancel")

	LabelSetText(WindowColorDialog.."ColorNewLabel", L"new")
	LabelSetFont(WindowColorDialog.."ColorNewLabel", "font_clear_tiny", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorNewLabel", 255, 255, 255)

	LabelSetText(WindowColorDialog.."ColorCurrentLabel", L"current")
	LabelSetFont(WindowColorDialog.."ColorCurrentLabel", "font_clear_tiny", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorCurrentLabel", 255, 255, 255)

	LabelSetText(WindowColorDialog.."ColorCurrentHueLabel", L"H:")
	LabelSetFont(WindowColorDialog.."ColorCurrentHueLabel", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorCurrentHueLabel", 255, 255, 255)
	LabelSetText(WindowColorDialog.."ColorCurrentHueMetrics", L"degree")
	LabelSetFont(WindowColorDialog.."ColorCurrentHueMetrics", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorCurrentHueMetrics", 255, 255, 255)
	LabelSetText(WindowColorDialog.."ColorCurrentSaturationLabel", L"S:")
	LabelSetFont(WindowColorDialog.."ColorCurrentSaturationLabel", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorCurrentSaturationLabel", 255, 255, 255)
	LabelSetText(WindowColorDialog.."ColorCurrentSaturationMetrics", L"%")
	LabelSetFont(WindowColorDialog.."ColorCurrentSaturationMetrics", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorCurrentSaturationMetrics", 255, 255, 255)
	LabelSetText(WindowColorDialog.."ColorCurrentBrightnessLabel", L"B:")
	LabelSetFont(WindowColorDialog.."ColorCurrentBrightnessLabel", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorCurrentBrightnessLabel", 255, 255, 255)
	LabelSetText(WindowColorDialog.."ColorCurrentBrightnessMetrics", L"%")
	LabelSetFont(WindowColorDialog.."ColorCurrentBrightnessMetrics", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorCurrentBrightnessMetrics", 255, 255, 255)

	LabelSetText(WindowColorDialog.."ColorCurrentRedLabel", L"R:")
	LabelSetFont(WindowColorDialog.."ColorCurrentRedLabel", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorCurrentRedLabel", 255, 255, 255)
	LabelSetText(WindowColorDialog.."ColorCurrentGreenLabel", L"G:")
	LabelSetFont(WindowColorDialog.."ColorCurrentGreenLabel", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorCurrentGreenLabel", 255, 255, 255)
	LabelSetText(WindowColorDialog.."ColorCurrentBlueLabel", L"B:")
	LabelSetFont(WindowColorDialog.."ColorCurrentBlueLabel", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	LabelSetTextColor(WindowColorDialog.."ColorCurrentBlueLabel", 255, 255, 255)

	LabelSetText(WindowColorDialog.."SliderBarAlphaLabel", L"Alpha")
end

--[[function Addon.ShutdownWindowColorDialog()
	-- Second step: destroy color dialog window
	if DoesWindowExist(WindowColorDialog) then
		DestroyWindow(WindowColorDialog)
	end
end]]--

--------------------------------------------------------------
-- function UpdateWindowSettings()
-- Description:
--------------------------------------------------------------
local function UpdateAlphaSliderBar()

	-- First step: update slider bar and information
	SliderBarSetCurrentPosition(WindowColorDialog.."SliderBarAlpha", NewColorAlpha)
	LabelSetText(WindowColorDialog.."SliderBarAlphaEdit",	towstring(string.sub(NewColorAlpha, 1, 6)))

	-- Second step: set controls visibility
	WindowSetShowing(WindowColorDialog.."SliderBarAlpha", CurrentColorType == Addon.ColorTypes.COLOR_TYPE_RGBA)
	WindowSetShowing(WindowColorDialog.."SliderBarAlphaEdit", CurrentColorType == Addon.ColorTypes.COLOR_TYPE_RGBA)
	WindowSetShowing(WindowColorDialog.."SliderBarAlphaLabel", CurrentColorType == Addon.ColorTypes.COLOR_TYPE_RGBA)
end

local function UpdateRGBEdits()

	-- First step: set edit boxes values
	IsUpdatingEdits = true
	TextEditBoxSetText(WindowColorDialog.."ColorCurrentRed", towstring(math.floor(NewColorRed)))
	TextEditBoxSetText(WindowColorDialog.."ColorCurrentGreen", towstring(math.floor(NewColorGreen)))
	TextEditBoxSetText(WindowColorDialog.."ColorCurrentBlue", towstring(math.floor(NewColorBlue)))
	IsUpdatingEdits = false
end

local function UpdateHSBEdits()

	-- First step: set edit boxes values
	IsUpdatingEdits = true
	TextEditBoxSetText(WindowColorDialog.."ColorCurrentHue", towstring(math.floor(NewColorHue)))
	TextEditBoxSetText(WindowColorDialog.."ColorCurrentSaturation", towstring(math.floor(NewColorSaturation)))
	TextEditBoxSetText(WindowColorDialog.."ColorCurrentBrightness", towstring(math.floor(NewColorBrightness)))
	IsUpdatingEdits = false
end

local function SetColorPointerPosition(X, Y)
	WindowClearAnchors(WindowColorDialog.."ColorPointer")
	WindowAddAnchor(WindowColorDialog.."ColorPointer", "topleft", WindowColorDialog.."Color", "center", X, Y)
end

local function UpdateColorSelector()

	-- First step: get color window dimensions
	local GUIWindowWidth, GUIWindowHeight = WindowGetDimensions(WindowColorDialog.."Color")

	-- Second step: set color pointer position
	local ColorPointerX = (NewColorHue * (GUIWindowWidth-1)) / 360
	local ColorPointerY = (GUIWindowHeight-1) - (NewColorBrightness * (GUIWindowHeight-1)) / 100
	SetColorPointerPosition(ColorPointerX, ColorPointerY)

	-- Third step: set saturation
	SliderBarSetCurrentPosition(WindowColorDialog.."SliderBarSaturation", NewColorSaturation / 100)
	WindowSetAlpha(WindowColorDialog.."ColorBlack", NewColorSaturation / 100)
end

local function UpdateColorPreview()
	-- First step: update color preview (new color field) 
	WindowSetTintColor(WindowColorDialog.."ColorNewForeground", NewColorRed, NewColorGreen, NewColorBlue)
end

function Addon.UpdateWindowSettings()

	-- First step: update settings window layer
	WindowSetLayer(WindowColorDialog, CurrentLayer)

	-- Second step: set new colors 
	NewColorRed			= CurrentColorRed
	NewColorGreen		= CurrentColorGreen
	NewColorBlue		= CurrentColorBlue
	NewColorAlpha		= CurrentColorAlpha

	-- Third step: convert to HSB
	NewColorHue,
	NewColorSaturation,
	NewColorBrightness	= Addon.RGB2HSB(NewColorRed, NewColorGreen, NewColorBlue)

	-- Fourth step:
	WindowSetTintColor(WindowColorDialog.."ColorCurrentForeground", CurrentColorRed, CurrentColorGreen, CurrentColorBlue)
	UpdateColorPreview()

	-- Fifth step: update color selector by HSB
	UpdateColorSelector()

	-- Sixth step: update HSB edit fields
	UpdateHSBEdits()

	-- Seventh step: update RGB edit fields
	UpdateRGBEdits()

	-- Final step: update alpha slider bar
	UpdateAlphaSliderBar()
end

--------------------------------------------------------------
-- function ShowWindowColorDialog()
-- Description:
--------------------------------------------------------------
function Addon.ShowWindowColorDialog()

	-- First step: check for window
	if not DoesWindowExist(WindowColorDialog) then
		Addon.InitializeWindowColorDialog()
	end

	-- Second step: update fields with the current configuration 
	Addon.UpdateWindowSettings()

	-- Final step: show everything
	WindowSetShowing(WindowColorDialog, true)
end

--------------------------------------------------------------
-- function HideWindowColorDialog()
-- Description:
--------------------------------------------------------------
function Addon.HideWindowColorDialog()

	-- Final step: hide window
	WindowSetShowing(WindowColorDialog, false)
end

--------------------------------------------------------------
-- function RGB2HSB()
-- Description: ported from the http://www.rags-int-inc.com/PhotoTechStuff/AcrCalibration/RGB2HSB.html
--------------------------------------------------------------
function Addon.RGB2HSB(Red, Green, Blue)

	-- First step: define locals
	local min = math.min(Red, Green, Blue)
	local max = math.max(Red, Green, Blue)
	local delta = max - min
	local hue = 0
	local sat = 0
	local lumin = 0

	-- Second step: check for maximum RGB value
	if max ~= 0 then

		-- Third step: calculate saturation & luminence
		sat = delta / max 
		lumin = max / 255

		-- Fourth step: calculate hue
		if delta ~= 0 then
			if Red==max then
				hue = (Green - Blue) / delta
			elseif Green==max then
				hue = 2 + ((Blue - Red) / delta)
			else
				hue = 4 + ((Red - Green) / delta)
			end
		end
		hue = hue * 60
		if hue < 0 then
			hue = hue + 360
		end
	end

	-- Final step: return result
	return math.ceil(hue), math.ceil(sat * 100), math.ceil(lumin * 100)
end

--------------------------------------------------------------
-- function HSB2RGB()
-- Description: ported from the http://www.easyrgb.com/index.php?X=MATH
--------------------------------------------------------------
function Addon.HSB2RGB(H, S, B)

	-- First step: define locals
	local Red, Green, Blue

	-- Second step: normalize to 1
	H = H/360
	S = S/100
	B = B/100

	-- Third step: check for saturaetion
	if S == 0 then

		Red = B * 255
		Green = B * 255
		Blue = B * 255

	else

		local var_h = H * 6

		if var_h == 6 then
			var_h = 0		-- H must be < 1
		end

		local var_i = math.floor(var_h)				 -- Or ... var_i = floor( var_h )
		local var_1 = B * (1 - S)
		local var_2 = B * (1 - S * (var_h - var_i))
		local var_3 = B * (1 - S * (1 - (var_h - var_i)))

		if var_i == 0 then
			var_r = B
			var_g = var_3
			var_b = var_1
		elseif var_i == 1 then
			var_r = var_2
			var_g = B
			var_b = var_1
		elseif var_i == 2 then
			var_r = var_1
			var_g = B
			var_b = var_3
		elseif var_i == 3 then
			var_r = var_1
			var_g = var_2
			var_b = B
		elseif var_i == 4 then
			var_r = var_3
			var_g = var_1
			var_b = B
		else
			var_r = B
			var_g = var_1
			var_b = var_2
		end

		-- : normalize to 255
		Red = var_r * 255
		Green = var_g * 255
		Blue = var_b * 255
	end

	-- Final step: return result
	return Red, Green, Blue
end

local function SendNewColor()

	-- First step: send color information to the client application
	CurrentCallbackFunction(CurrentCallbackOwner, Addon.Events.COLOR_EVENT_UPDATED, {Red = NewColorRed, Green = NewColorGreen, Blue = NewColorBlue, Alpha = NewColorAlpha})
end

local function SendCurrentColor()

	-- First step: send color information to the client application
	CurrentCallbackFunction(CurrentCallbackOwner, Addon.Events.COLOR_EVENT_UPDATED, {Red = CurrentColorRed, Green = CurrentColorGreen, Blue = CurrentColorBlue, Alpha = CurrentColorAlpha})
end

-------------------------------------------------------------
-- Window Events
--------------------------------------------------------------
local function OnMouseMove(MouseX, MouseY)

	-- First step: check for moving pointer flag
	if IsMovingColorPointer then

		-- Second step: get locals
		local ScreenWindowX, ScreenWindowY = WindowGetScreenPosition(WindowColorDialog.."Color")
		local GUIColorX = ScreenMouseX / InterfaceCore.GetScale() - ScreenWindowX / InterfaceCore.GetScale()
		local GUIColorY = ScreenMouseY / InterfaceCore.GetScale() - ScreenWindowY / InterfaceCore.GetScale()
		local GUIWindowWidth, GUIWindowHeight = WindowGetDimensions(WindowColorDialog.."Color")

		-- Third step: fix coordinates
		if GUIColorX < 0 then
			GUIColorX = 0
		end
		if GUIColorX > GUIWindowWidth - 1 then
			GUIColorX = GUIWindowWidth - 1
		end
		if GUIColorY < 0 then
			GUIColorY = 0
		end
		if GUIColorY > GUIWindowHeight - 1 then
			GUIColorY = GUIWindowHeight - 1
		end

		-- Fourth step: set pointer position
		SetColorPointerPosition(GUIColorX, GUIColorY)

		-- Fifth step: set new HSB
		NewColorHue				= (GUIColorX * 360) / (GUIWindowWidth - 1)
		NewColorBrightness		= 100 - (GUIColorY * 100) / (GUIWindowHeight - 1)

		-- Sixth step: set new RGB
		NewColorRed,
		NewColorGreen,
		NewColorBlue			= Addon.HSB2RGB(NewColorHue, NewColorSaturation, NewColorBrightness)

		-- Seventh step: update fields
		UpdateHSBEdits()
		UpdateRGBEdits()
		UpdateColorPreview()

		-- Final step: send new color to the client application
		SendNewColor()
	end
end

function Addon.OnUpdate( timePassed )

	-- First step: check for mouse position changed
	if (ScreenMouseX ~= SystemData.MousePosition.x or ScreenMouseY ~= SystemData.MousePosition.y) then

		-- Second step: save new position
		ScreenMouseX = SystemData.MousePosition.x
		ScreenMouseY = SystemData.MousePosition.y

		-- Final step: emulate mouse move event
		OnMouseMove(ScreenMouseX, ScreenMouseY)
	end
end

function Addon.OnLButtonDownColor()
	
	-- First step: inform everyone we are starting to move pointer
	IsMovingColorPointer = true

	-- Second step: emulate OnMouseMove event
	OnMouseMove(ScreenMouseX, ScreenMouseY)
	
	RegisterEventHandler(SystemData.Events.L_BUTTON_UP_PROCESSED, MAJOR..".OnLButtonUpColor")
end

function Addon.OnLButtonUpColor()

	-- First step: inform everyone we are not moving pointer
	IsMovingColorPointer = false
	
	UnregisterEventHandler(SystemData.Events.L_BUTTON_UP_PROCESSED, MAJOR..".OnLButtonUpColor")
end

function Addon.OnTextChangedEdit()

	-- First step: check for IsUpdatingEdits
	if IsUpdatingEdits then 
		return
	end

	-- Second step: get edit window name
	local EditWindow = SystemData.ActiveWindow.name

	-- Third step: check for the value type. It should be numeric only
	local Value = tonumber(WStringToString(TextEditBoxGetText(EditWindow)))
	if Value == nil
	then
		return
	end

	-- Fourth step: check for right edit window name
	if EditWindow == WindowColorDialog.."ColorCurrentHue" then

		-- : check for right range
		if Value < 0 or Value > 360 then
			return
		end

		-- : set new HSB
		NewColorHue			= Value

		-- : set new RGB
		NewColorRed,
		NewColorGreen,
		NewColorBlue		= Addon.HSB2RGB(NewColorHue, NewColorSaturation, NewColorBrightness)

	elseif EditWindow == WindowColorDialog.."ColorCurrentSaturation" then

		-- : check for right range
		if Value < 0 or Value > 100 then
			return
		end

		-- : set new HSB
		NewColorSaturation	= Value

		-- : set new RGB
		NewColorRed,
		NewColorGreen,
		NewColorBlue		= Addon.HSB2RGB(NewColorHue, NewColorSaturation, NewColorBrightness)

	elseif EditWindow == WindowColorDialog.."ColorCurrentBrightness" then

		-- : check for right range
		if Value < 0 or Value > 100 then
			return
		end

		-- : set new HSB
		NewColorBrightness	= Value

		-- : set new RGB
		NewColorRed,
		NewColorGreen,
		NewColorBlue		= Addon.HSB2RGB(NewColorHue, NewColorSaturation, NewColorBrightness)

	elseif EditWindow == WindowColorDialog.."ColorCurrentRed" then

		-- : check for right range
		if Value < 0 or Value > 255 then
			return
		end

		-- : set new RGB
		NewColorRed			= Value

		-- : set new HSB
		NewColorHue,
		NewColorSaturation,
		NewColorBrightness	= Addon.RGB2HSB(NewColorRed, NewColorGreen, NewColorBlue)

	elseif EditWindow == WindowColorDialog.."ColorCurrentGreen" then

		-- : check for right range
		if Value < 0 or Value > 255 then
			return
		end

		-- : set new RGB
		NewColorGreen		= Value

		-- : set new HSB
		NewColorHue,
		NewColorSaturation,
		NewColorBrightness	= Addon.RGB2HSB(NewColorRed, NewColorGreen, NewColorBlue)

	elseif EditWindow == WindowColorDialog.."ColorCurrentBlue" then

		-- : check for right range
		if Value < 0 or Value > 255 then
			return
		end

		-- : set new RGB
		NewColorBlue		= Value

		-- : set new HSB
		NewColorHue,
		NewColorSaturation,
		NewColorBrightness	= Addon.RGB2HSB(NewColorRed, NewColorGreen, NewColorBlue)

	end

	-- Fifth step: update fields
	UpdateColorSelector()
	UpdateHSBEdits()
	UpdateRGBEdits()
	UpdateColorPreview()

	-- Final step: send new color to the client application
	SendNewColor()
end

function Addon.OnSlide( slidePos )

	local SliderWindow = SystemData.ActiveWindow.name

	if SliderWindow == WindowColorDialog.."SliderBarSaturation" then

		-- : set alpha for the ColorBlack
		WindowSetAlpha(WindowColorDialog.."ColorBlack", slidePos)

		-- : set new HSB
		NewColorSaturation		= slidePos * 100

		-- : set new RGB
		NewColorRed,
		NewColorGreen,
		NewColorBlue			= Addon.HSB2RGB(NewColorHue, NewColorSaturation, NewColorBrightness)

		-- : update fields
		UpdateHSBEdits()
		UpdateRGBEdits()
		UpdateColorPreview()

		-- : send new color to the client application
		SendNewColor()

	elseif SliderWindow == WindowColorDialog.."SliderBarAlpha" then

		-- : set alpha information
		LabelSetText(WindowColorDialog.."SliderBarAlphaEdit", towstring(string.sub(slidePos, 1, 6)))

		-- : set new A
		NewColorAlpha			= slidePos

		-- : send new color to the client application
		SendNewColor()

	end
end

function Addon.OnLButtonUpButtonOK()

	-- First step: close dialog window and save color
	Addon.API_CloseDialog(true)
end

function Addon.OnLButtonUpButtonCancel()

	-- First step: close dialog window and don't save a color
	Addon.API_CloseDialog(false)
end

--------------------------------------------------------------------------------
--							Addon API							  --
--------------------------------------------------------------------------------

--------------------------------------------------------------
-- function API_GetLink
-- Description:
--------------------------------------------------------------
function Addon.API_GetLink()

	-- Final step: return link information
	return CurrentCallbackOwner, CurrentCallbackFunction
end

--------------------------------------------------------------
-- function API_OpenDialog
-- Description:
--------------------------------------------------------------
function Addon.API_OpenDialog(CallbackOwner, CallbackFunction, SaveColor, ColorR, ColorG, ColorB, ColorA, Layer, ColorType)

	-- First step: check for input data
	if CallbackOwner == nil or CallbackFunction == nil then
		return false
	end

	-- Second step: close dialog window and don't save a color settings
	Addon.API_CloseDialog(SaveColor)

	-- Third step: set new link
	CurrentCallbackOwner	= CallbackOwner
	CurrentCallbackFunction	= CallbackFunction
	CurrentColorRed			= ColorR or 0
	CurrentColorGreen		= ColorG or 0
	CurrentColorBlue		= ColorB or 0
	CurrentColorAlpha		= ColorA or 1
	CurrentLayer			= Layer or Window.Layers.DEFAULT
	CurrentColorType		= ColorType or Addon.ColorTypes.COLOR_TYPE_RGBA

	-- Fourth step: show settings window
	Addon.ShowWindowColorDialog()

	-- Final step: return result
	return true
end

--------------------------------------------------------------
-- function API_CloseDialog
-- Description:
--------------------------------------------------------------
function Addon.API_CloseDialog(SaveColor)

	-- First step: check for link
	if CurrentCallbackOwner == nil or CurrentCallbackFunction == nil then
		return false
	end

	-- Second step: get locals
	local LocalSaveColor = SaveColor or false

	-- Third step: check for the SaveColor flag
	if LocalSaveColor then

		-- Fourth step: send new color to the client application
		SendNewColor()
	else

		-- Fifth step: send current color to the client application
		SendCurrentColor()
	end

	-- Sixth step: hide dialog window
	Addon.HideWindowColorDialog()

	-- Seventh step: send OnClose event to the client application
	CurrentCallbackFunction(CurrentCallbackOwner, Addon.Events.COLOR_EVENT_CLOSED, {ColorSaved = LocalSaveColor})

	-- Eight step: clear link information
	CurrentCallbackOwner	= nil
	CurrentCallbackFunction	= nil

	-- Final step: return result
	return true
end

_G[MAJOR] = Addon