<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<UiMod name="LibColors" version="1" date="12/12/2011" >
	<VersionSettings gameVersion="1.4.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
	<Author name="Wothor|Philosound" email="" />
	<Description text="Global Color Preset Storage" />

	<Dependencies>
		<Dependency name="EASystem_Utils" optional="false" forceEnable="true" />
	</Dependencies>

	<Files>
		<File name="libs/LibStub.lua" />
		<File name="libs/LibGUI.lua" />
		<File name="libs/LibColorDialog/LibColorDialog.xml" />
		<File name="libs/LibColorDialog/LibColorDialog.lua" />
		<File name="LibColors_FactoryPresets.lua" />
		<File name="LibColors.xml" />
		<File name="LibColors.lua" />
	</Files>
	
	<OnInitialize>
		<CallFunction name="LibColors.Initialize" />
	</OnInitialize>

	<OnUpdate />

	<OnShutdown>
		<CallFunction name="LibColors.Shutdown" />
	</OnShutdown>
	
	<SavedVariables>
		<SavedVariable name="LibColors.CurrentConfiguration" global="true"/>
	</SavedVariables>
	
	<WARInfo>
		<Categories>
			<Category name="SYSTEM" />
			<Category name="DEVELOPMENT" />
			<Category name="OTHER" />
		</Categories>
		<Careers>
			<Career name="BLACKGUARD" />
			<Career name="WITCH_ELF" />
			<Career name="DISCIPLE" />
			<Career name="SORCERER" />
			<Career name="IRON_BREAKER" />
			<Career name="SLAYER" />
			<Career name="RUNE_PRIEST" />
			<Career name="ENGINEER" />
			<Career name="BLACK_ORC" />
			<Career name="CHOPPA" />
			<Career name="SHAMAN" />
			<Career name="SQUIG_HERDER" />
			<Career name="WITCH_HUNTER" />
			<Career name="KNIGHT" />
			<Career name="BRIGHT_WIZARD" />
			<Career name="WARRIOR_PRIEST" />
			<Career name="CHOSEN" />
			<Career name="MARAUDER" />
			<Career name="ZEALOT" />
			<Career name="MAGUS" />
			<Career name="SWORDMASTER" />
			<Career name="SHADOW_WARRIOR" />
			<Career name="WHITE_LION" />
			<Career name="ARCHMAGE" />
		</Careers>
	</WARInfo>
</UiMod>
</ModuleFile>
